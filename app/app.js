const { getContacts, postContacts, putContacts, deleteContacts } = require("./services/ContactService");

const api = app => {
	app.get("/contacts", getContacts);
	app.post("/contacts", postContacts);
	app.put("/contacts/:id", putContacts);
	app.delete("/contacts/:id", deleteContacts)
};

module.exports = api;