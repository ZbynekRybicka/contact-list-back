const { db } = require('../../ext');

module.exports = (collection, _id, cb) => {
	db.request((err, conn) => {
		if (err) {
			throw err;
		}
		conn.collection(collection).deleteOne({ _id }, cb);
	});
}