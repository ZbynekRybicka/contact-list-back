const select = require('./select');
const insert = require('./insert');
const update = require('./update');
const del = require('./del');

module.exports = { select, insert, update, del };