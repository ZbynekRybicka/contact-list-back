const { db } = require('../../ext');

module.exports = (collection, _id, body, cb) => {
	db.request((err, conn) => {
		if (err) {
			console.log(err);
			throw err;
		}
		delete body._id;
		conn.collection(collection).updateOne({ _id: new db.ObjectId(_id) }, { $set: body }, cb);
	});
}