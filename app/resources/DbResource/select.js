const { db } = require('../../ext');

module.exports = (collection, params, cb) => {
	db.request((err, conn) => {
		if (err) {
			throw err;
		}
		conn.collection(collection).find(params).toArray(cb);
	});
}