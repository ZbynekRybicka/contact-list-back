const { db } = require('../../ext');

module.exports = (collection, body, cb) => {
	db.request((err, conn) => {
		if (err) {
			throw err;
		}
		conn.collection(collection).insertOne(body, (err, result) => {
			if (err) {
				cb(err, null);
			} else {
				cb(err, result.insertedId);
			}
		});
	});
	
}