const { insert } = require('../../resources/DbResource');

module.exports = (req, res) => {
	try {
		insert('contacts', req.body, (err, id) => {
			if (err) {
				console.log(err);
				return res.status(500).json(err);
			} else {
				return res.status(201).json(id);
			}
		});
	} catch (err) {
		console.log(err);
		return res.status(500).json(err);
	}
}