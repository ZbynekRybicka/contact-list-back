const { update } = require('../../resources/DbResource');

module.exports = (req, res) => {
	try {
		update('contacts', req.params.id, req.body, err => {
			if (err) {
				console.log(err);
				return res.status(500).json(err);
			} else {
				return res.status(204).end();
			}
		});
	} catch (err) {
		console.log(err);
		return res.status(500).json(err);
	}
}
