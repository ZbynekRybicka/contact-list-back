const getContacts = require('./getContacts');
const postContacts = require('./postContacts');
const putContacts = require('./putContacts');
const deleteContacts = require('./deleteContacts');

module.exports = { getContacts, postContacts, putContacts, deleteContacts };
