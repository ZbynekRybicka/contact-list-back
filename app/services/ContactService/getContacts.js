const { select } = require('../../resources/DbResource');

module.exports = (req, res) => {
	try {
		select('contacts', req.query, (err, result) => {
			if (err) {
				console.log(err);
				return res.status(500).json(err);
			} else {
				return res.status(200).json(result);
			}
		});
	} catch (err) {
		console.log(err);
		return res.status(500).json(err);
	}
}