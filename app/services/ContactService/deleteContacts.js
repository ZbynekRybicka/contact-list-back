const { del } = require('../../resources/DbResource');

module.exports = (req, res) => {
	try {
		del('contacts', req.params.id, err => {
			if (err) {
				console.log(err);
				return res.status(500).json(err);
			} else {
				return res.status(204).end();
			}
		});
	} catch (err) {
		console.log(err);
		return res.status(500).json(err);
	}
}