const mongoDb = require('mongoDb')
const config = require('../../config.js').mongoDb;

const { MongoClient } = mongoDb
module.exports = {
	request: cb => {
		MongoClient.connect(config.url, (err, client) => {
			const db = err ? null : client.db(config.database);
			cb(err, db);
			client.close();
		})
	},
	ObjectId: mongoDb.ObjectId,
}

