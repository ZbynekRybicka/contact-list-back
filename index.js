const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config')
const api = require('./app/app');
const app = express();

app.use(bodyParser.json())
app.use(cors());
api(app);

app.listen(config.port, () => {
	console.log("Application runs on port " + config.port + ".");
});